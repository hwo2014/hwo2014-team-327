package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import noobbot.Piece.PieceType;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class Main {

	ArrayList<Group> groups;
	ArrayList<Piece> objPieces;
	int lastSwitchIndex;
	String lastSwitchDirection;
	int lastPieces;
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        lastSwitchIndex=-1;
        lastSwitchDirection="Left";
        lastPieces = 0;
        
        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	decide(msgFromServer);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	loadRaceData(msgFromServer);
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
            	System.out.println(msgFromServer.msgType);
               send(new Ping());
             // decide(msgFromServer);
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    @SuppressWarnings("unchecked")
	private void decide(MsgWrapper msgCarPositions){
    
      float throt=0.6f;
      
     
    
      ArrayList<Object> positions=(ArrayList<Object>) msgCarPositions.data;
      ArrayList<CarPosition> positionObjs=new ArrayList<CarPosition>();
      CarPosition carPositionObj=null;
      for(int i=0;i<positions.size();i++){
    	
		LinkedTreeMap<String,Object> carPosition=(LinkedTreeMap<String,Object> ) positions.get(i);
    	 
    	String name=(String) ((LinkedTreeMap<String,Object>) carPosition.get("id")).get("name");
    	String color=(String) ((LinkedTreeMap<String,Object>) carPosition.get("id")).get("color");
    	Double angle=(Double) carPosition.get("angle");
    	
    	Double position=(Double) ((LinkedTreeMap<String,Object>) carPosition.get("piecePosition")).get("pieceIndex");
    	Double distance=(Double) ((LinkedTreeMap<String,Object>) carPosition.get("piecePosition")).get("inPieceDistance");
    	Double lap=(Double) ((LinkedTreeMap<String,Object>) carPosition.get("piecePosition")).get("lap");
    	Double startIndex=(Double) ((LinkedTreeMap<String,LinkedTreeMap<String,Object>>) carPosition.get("piecePosition")).get("lane").get("startLaneIndex");
    	Double endIndex=(Double) ((LinkedTreeMap<String,LinkedTreeMap<String,Object>>) carPosition.get("piecePosition")).get("lane").get("endLaneIndex");
    	
//    	System.out.println(String.format("%s %s %f %f %f %f %f %f",name,color,angle,position,distance,lap,startIndex,endIndex));
//    	System.out.println(carPosition.toString());
    	carPositionObj=new CarPosition(name, color, position, distance, startIndex, endIndex, lap, angle);
    	positionObjs.add(carPositionObj);
      } 
      
    
      
      Piece currentPiece=null,nextPiece = null;

      if(carPositionObj!=null){
    	  currentPiece=objPieces.get((int) carPositionObj.piecePosition.pieceIndex);
    	  nextPiece=objPieces.get(((int) carPositionObj.piecePosition.pieceIndex+1)%objPieces.size());
    	  
    	  if(currentPiece.type==PieceType.STRAIGHT)
    		  lastPieces++;
    	  else {
    		 lastPieces=0;
    	  }  
      }
      
      currentPiece.isDriven=true;
      
     // System.out.println(currentPiece.GroupNo);
      Group group=groups.get(currentPiece.GroupNo);
      
      if(group.distance==0)
      {
    	  throt=0.65f;
    	  Group nextGroup =groups.get(nextPiece.GroupNo);
    	  
    	  if(nextGroup.type==PieceType.STRAIGHT && nextGroup.distance>300) throt=1.0f;
    	  else if(nextGroup.type==PieceType.STRAIGHT && nextGroup.distance>200) throt=0.9f;
    	  else if(Math.abs(currentPiece.angle)<25 && nextPiece.type==PieceType.BEND && Math.abs(nextPiece.angle)<25) throt=0.85f;
    	  else if(Math.abs(currentPiece.angle)<25) throt=0.721f;
    	  
    	 
    	 
      }
      else if(group.type==PieceType.STRAIGHT)
      {
    	
    	 if(ReduceSpeed(group,carPositionObj)){
    		 throt=0.62f;
    	 }
    	 else 
    	 {
    		  if(group.distance<201)
    			 throt=0.65f;
    			 else throt=1.0f;
    		 
    	 }
      }
      
      System.out.println(" Throttle: " + throt + " ");
      send(new Throttle(throt));
    }
    
    public boolean ReduceSpeed(Group  group,CarPosition carPosition){
    
    	int distanceCovered=0;
    	int minusDistance=0;
    	int i=group.startIndex;
    	

    	while(i!=carPosition.piecePosition.pieceIndex)
    	{
    		 if(objPieces.get(i).isDriven)
    			distanceCovered+=objPieces.get(i).length;
    		else minusDistance+=objPieces.get(i).length;
    		
    		i=(i+1)%objPieces.size();
    	}
    	
    	distanceCovered+=carPosition.piecePosition.inPieceDistance;
    	
    	System.out.print("Group : " + objPieces.get((int) carPosition.piecePosition.pieceIndex).GroupNo + " Covered :" +distanceCovered + " Group Distance :" + (group.distance-minusDistance)  + " Minus: " + minusDistance + " ");
    	float percentage=((float)distanceCovered/((float)group.distance-(float)minusDistance)) * 100;
    	System.out.print(percentage+ "% ");
    	if(carPosition.piecePosition.lap==0.0d)
    	{
    		if(percentage >80.0f)
    		return true;
    	}
    	else if(carPosition.piecePosition.lap>0.0d){
    		if(percentage >65.0f)
        		return true;
    	}
    	
    	return false;
    }
    
    @SuppressWarnings("unchecked")
	private void loadRaceData(MsgWrapper msgRaceData){

		LinkedTreeMap<String, LinkedTreeMap<String, LinkedTreeMap<String,Object>>> data= (LinkedTreeMap<String, LinkedTreeMap<String, LinkedTreeMap<String,Object>>>)msgRaceData.data;
  
		ArrayList<Object>  pieces= (ArrayList<Object>) data.get("race").get("track").get("pieces");
    	 objPieces=new ArrayList<Piece>();
    	
    	double length,radius,angle;
    	boolean hasSwitch;
    	for(int i=0;i<pieces.size();i++)
    	{
    		
    		LinkedTreeMap<String,Object> pc=(LinkedTreeMap<String,Object>)pieces.get(i);
    		length=(Double) (pc.get("length")!=null? pc.get("length"):0.0);	 
    		radius=(Double) (pc.get("radius")!=null? pc.get("radius"):0.0);
       		angle=(Double) (pc.get("angle")!=null? pc.get("angle"):0.0);
    		hasSwitch=(Boolean) (pc.get("switch")!=null? pc.get("switch"):false);
    		objPieces.add(new Piece(length,radius,angle,hasSwitch));
    	//	System.out.println(pieces.get(i).toString());
    	}
    	
    	int groupNo=0,j,groupDistance=0,startIndex,endIndex;
    	for( j=0;j<objPieces.size();j++)
    	{
    		if(objPieces.get(j).type!=objPieces.get(j+1).type) break;
    	}
    	startIndex=endIndex=j;
    	groups=new ArrayList<Group>();
    	int noGrouped=0,i,k,noOfPieces=0;
    	Piece currentPiece=null ,nextPiece;
    	for( i=j,k=0;k<objPieces.size();k++,i=(i+1) % objPieces.size()){
			currentPiece = objPieces.get(i);
			nextPiece=objPieces.get((i+1)%objPieces.size());
			currentPiece.GroupNo=groupNo;
			groupDistance+=currentPiece.length;
			noOfPieces++;
			if(currentPiece.type!=nextPiece.type)
			{
				endIndex=i;
				Group group=new Group(startIndex, i, currentPiece.type, groupDistance,noOfPieces);
				System.out.println(groupNo + " " + group.distance + " " + group.startIndex + " " + group.endIndex);
				groups.add(group);
				groupNo++;
				groupDistance=0;
				endIndex=startIndex=(i+1) % objPieces.size();
				noGrouped++;
				noOfPieces=0;
				
			}	
    	}
    	if(noGrouped!=objPieces.size())
    	{
    		Group group=new Group(startIndex, i, currentPiece.type, groupDistance,noOfPieces);
    		groups.add(group);
    	}
    }

}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Switch extends SendMsg {
    private String value;

    public Switch(String string) {
        this.value = string;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}


class Piece{
	double length;
	boolean hasSwitch;
	double radius;
	double angle;
	PieceType type;
	int GroupNo;
	public enum PieceType{ STRAIGHT,BEND }
	boolean isDriven;
	
	public Piece(double length,double radius,double angle,boolean hasSwitch){
		this.length=length;
		this.radius=radius;
		this.angle=angle;
		this.hasSwitch=hasSwitch;
		type=radius>0.0f? PieceType.BEND:PieceType.STRAIGHT;
		GroupNo=-1;
		isDriven=false;
	}
	
}

class Group{
	int startIndex;
	int endIndex;
	PieceType type;
	int distance;
	int noOfPieces;
	public Group(int startIndex,int endIndex,PieceType type,int distance,int noOfPieces)
	{
		this.startIndex=startIndex;
		this.endIndex=endIndex;
		this.type=type;
		this.distance=distance;
		this.noOfPieces=noOfPieces;
	}
}

class Track{
	Piece[] pieces;
	int lanes;
}

class CarId{
	String name;
	String color;
	public CarId(String name,String color){
		this.name=name;
		this.color=color;
	}
}

class Lane{
	double startLaneIndex;
	double endLaneIndex;
	public Lane(double startIndex,double endIndex){
		this.startLaneIndex=startIndex;
		this.endLaneIndex=endIndex;
	}
}

class PiecePosition{
	double pieceIndex;
	double inPieceDistance;
	Lane lane;
	double lap;
	public PiecePosition(double pieceIndex,double inPieceDistance,double startIndex,double endIndex,double lap){
		this.lane=new Lane(startIndex,endIndex);
		this.pieceIndex=pieceIndex;
		this.inPieceDistance=inPieceDistance;
		this.lap=lap;
	}
	
}

class CarPosition{
	CarId carId;
	double angle;
	PiecePosition piecePosition;
	public CarPosition(String name,String color,double pieceIndex,double inPieceDistance,double startIndex,double endIndex,double lap,double angle){
		this.piecePosition= new PiecePosition( pieceIndex, inPieceDistance, startIndex, endIndex, lap);
		this.carId=new CarId(name,color);
		this.angle=angle;
	}
	
	
}


